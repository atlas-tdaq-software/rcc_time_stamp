/************************************************************************/
/*                                                                      */
/*   file: tstamp.c                                                     */
/*   author: Markus Joos, CERN-EP/ESS                                   */
/*                                                                      */
/*   This is the timestamping library.                                  */
/*                                                                      */
/*   History:                                                           */
/*   13.Aug.98  MAJO  created                                           */
/*   22.Sep.98  MAJO  Separate header file created                      */
/*   26.Feb.99  MAJO  additional run control functions added            */
/*    5.Mar.99  MAJO  more functions added                              */
/*   26.Jul.99  JOP   change algorithm in ts_duration                   */
/*   27.Jul.99  JOP   NGU  PC - i386 functions added                    */
/*    2.Aug.99  MAJO  Improved algorithm in ts_duration        		*/
/*   16.Aug.99  NGU   cpu frequency reading added for Linux             */
/*   13.Aug.01  MAJO  cpu frequency reading added for CES RIO3          */
/*                    IOM error system introduced                       */
/*   16.Jul.02  MAJO  Ported to RCC environment				*/
/*   23.Sep.02  MAJO  ts_wait added					*/
/*   19.Oct.04  MAJO  ts_wait_until, ts_offset and ts_compare added	*/
/*                                                                      */
/*******Copyright Ecosoft 2007 - Made from 80% recycled bytes************/ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sched.h>
#include <time.h>
#include <sys/file.h>
#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"


#ifdef __x86_64__
#define RDTSC(lo,hi)  __asm__ volatile  ("rdtsc":"=a" (lo), "=d" (hi));
#else
#define RDTSC(lo,hi) do { struct timespec t; clock_gettime(CLOCK_MONOTONIC_RAW, &t);  unsigned long res = t.tv_sec * 1000000000l + t.tv_sec;  hi = res >> 32; lo = res & 0xffffffff; } while(false);
#endif


// Globals
static int isopen = 0, got_freq = 0, tst0 = 0, tsmode[MAX_OPENS], tsnum[MAX_OPENS], tsmax[MAX_OPENS], tsactive[MAX_OPENS];
static double frequency_high, frequency_low, frequency_high_us, frequency_low_us;
static tstamp *ts[MAX_OPENS], *tsp, tzero;
static FILE *dfile;


/******************************/
TS_ErrorCode_t ts_get_freq(void)
/******************************/
{
  FILE *fd;
  int i1;
  float Frequency_low_us;
  char dumc[64];

#if __x86_64__
  // Determine the frequency of the clock 
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_get_freq: setting frequencies for Intel");
  if ((fd = fopen("/proc/cpuinfo", "r") ) == 0)
  { 
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_get_freq: cannot open the file cpuinfo"); 
    return(RCC_ERROR_RETURN(0, TSE_FILE)); 
  }
  
  for(i1 = 1; i1 < 60; i1++)
  {
    fscanf (fd, "%s ", dumc);
    if (strcmp(dumc, "MHz") == 0)
      break;
  }

  fscanf (fd, "%s ", dumc); 
  fscanf (fd, "%f", &Frequency_low_us);
  frequency_low_us = (double)Frequency_low_us;
  frequency_high_us = (frequency_low_us) / 4294967296.0;
  fclose(fd);

#else

  frequency_low_us = 1000.;
  frequency_high_us = (frequency_low_us) / 4294967296.0;

#endif

  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_get_freq: hi_us: " << frequency_high_us << " low_us: " << frequency_low_us);
  frequency_high = frequency_high_us * 1000000.0;
  frequency_low = frequency_low_us * 1000000.0;  

  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_get_freq: frequency_low    = " << frequency_low);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_get_freq: frequency_low_us = " << frequency_low_us);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_get_freq: frequency_high   = " << frequency_high);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_get_freq: frequency_high_us= " << frequency_high_us);

  got_freq = 1;
  DEBUG_TEXT(DFDB_RCCTS, 15, "ts_get_freq: setting frequencies done.");   
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/******************************************/
TS_ErrorCode_t ts_open(int size, int handle)
/******************************************/
{
  err_type ret;

  DEBUG_TEXT(DFDB_RCCTS, 15, "ts_open called");

  if(!isopen)
  {  
    ret = iom_error_init(P_ID_TS, packTS_err_get);
    if(ret)
      return(RCC_ERROR_RETURN(0, TSE_ERROR_FAIL));
  }

  if (!got_freq)
  {
    ret = ts_get_freq();
    if (ret)
    {
      DEBUG_TEXT(DFDB_RCCTS, 5, "ts_open: error in ts_get_freq()");
      return(RCC_ERROR_RETURN(ret, TSE_NO_FREQ));
    }
  }
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_open: frequency acquired");

  if (handle != 0)
  {  
    if (handle > MAX_OPENS || handle < 1)
      return(RCC_ERROR_RETURN(0, TSE_ILL_HANDLE));

    // Allocate an aray for the time stamps
    tsmax[handle] = size;
    if (size)
      ts[handle] = (tstamp *)malloc(size * sizeof(tstamp));
    else
    {
      DEBUG_TEXT(DFDB_RCCTS, 5, "ts_open: error with parameter <size>");
      return(RCC_ERROR_RETURN(0, TSE_ILL_SIZE));
    }

    tsactive[handle] = TS_STOP_TS;
    tsnum[handle] = 0;
    tsmode[handle] = TS_MODE_NORING;
  }

  isopen++;
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/********************************************/
TS_ErrorCode_t ts_mode(int handle, u_int mode)
/********************************************/
{
  int loop;
  
  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_mode: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));
  }   
  
  if (handle > MAX_OPENS || handle < 1)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_mode: Error=TSE_ILL_HANDLE");
    return(RCC_ERROR_RETURN(0, TSE_ILL_HANDLE));
  }
  
  if (mode != TS_MODE_RING && mode != TS_MODE_NORING)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_mode: Error=TSE_ILL_MODE");
    return(RCC_ERROR_RETURN(0, TSE_ILL_MODE));
  }
    
  tsmode[handle] = mode;

  if (mode == TS_MODE_RING)
  {
    for(loop = 0; loop < tsmax[handle]; loop++)
    {
      tsp = (tstamp *)&ts[handle][loop];
      tsp->low = 0;
      tsp->high = 0;
      tsp->data = 0;
    }
  }
  
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_mode: mode set to " << mode);
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/********************************************/
TS_ErrorCode_t ts_save(int handle, char *name)
/********************************************/
{
  // Write the time stamps from the array to an ASCII file and reset the array
  int res, loop, num;

  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_save: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));
  }
  
  if (handle > MAX_OPENS || handle < 1)
    return(RCC_ERROR_RETURN(0, TSE_ILL_HANDLE));
    
  if(tsmode[handle] == TS_MODE_RING || tsmax[handle])
  { 
    dfile = fopen(name,"w+");
    if (dfile == 0)
    {
      DEBUG_TEXT(DFDB_RCCTS, 5, "ts_save: Cannot open parameter file");
      return(RCC_ERROR_RETURN(0, TSE_PFILE));
    }

    // Save the frequencies
    fprintf(dfile, "%e\n", frequency_high);
    fprintf(dfile, "%e\n", frequency_low);
    DEBUG_TEXT(DFDB_RCCTS, 20, "ts_save: frequencies saved");

    if (tsmode[handle] == TS_MODE_RING)
      num = tsmax[handle];
    else
      num = tsnum[handle];
      
    RDTSC(tsp->low, tsp->high);

    DEBUG_TEXT(DFDB_RCCTS, 20, "ts_save: saving " << num << " data points"); 
    for(loop = 0; loop < num; loop++)
      fprintf(dfile, "%u %u %u\n", ts[handle][loop].high, ts[handle][loop].low, ts[handle][loop].data);

    res = fclose(dfile);
    if (res)
    {
      DEBUG_TEXT(DFDB_RCCTS, 5, "ts_save: Data file closed with error " << res);
      return(RCC_ERROR_RETURN(0, TSE_PFILE));
    }
    tsnum[handle] = 0;
  } 
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/*********************************/
TS_ErrorCode_t ts_close(int handle)
/*********************************/
{
  
  if (handle > MAX_OPENS || handle < 0)
    return(RCC_ERROR_RETURN(0, TSE_ILL_HANDLE));
    
  if (isopen > 0)
  {
    isopen--;
    if (handle)
      free((void *)ts[handle]);
  }
  else
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_close: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));
  }
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/*********************************************/
TS_ErrorCode_t ts_record(int handle, int udata)
/*********************************************/
{
  // Record a time stamp together with an user defined identifier if there is space left in the array

  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_record: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }

  if (tsactive[handle] == 0)
  {
    tsp = (tstamp *)&ts[handle][tsnum[handle]];
    tsp->data = udata;
    if (++tsnum[handle] >= tsmax[handle])
    {
      if (tsmode[handle] == TS_MODE_NORING)
        tsactive[handle] |= TS_FULL;
      else
        tsnum[handle] = 0;
    }
  }
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/***************************/
TS_ErrorCode_t ts_sett0(void)
/***************************/
{
  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_sett0: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }

  if (!tst0)
  {
    ts_clock(&tzero);
    tst0 = 1;
  }

  return(RCC_ERROR_RETURN(0, TSE_OK));
}
 

/*********************************/
TS_ErrorCode_t ts_start(int handle)
/*********************************/
{
  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_start: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }
  
  if (handle > MAX_OPENS || handle < 1)
    return(RCC_ERROR_RETURN(0, TSE_ILL_HANDLE));
    
  tsactive[handle] &= TS_START_TS;
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_start: time stamping enabled for handle " << handle);
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/*********************************/
TS_ErrorCode_t ts_pause(int handle)
/*********************************/
{
  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_pause: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }
  
  if (handle > MAX_OPENS || handle < 1)
    return(RCC_ERROR_RETURN(0, TSE_ILL_HANDLE));
    
  tsactive[handle] |= TS_STOP_TS;
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/**********************************/
TS_ErrorCode_t ts_resume(int handle)
/**********************************/
{
  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_resume: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }
  
  if (handle > MAX_OPENS || handle < 1)
    return(RCC_ERROR_RETURN(0, TSE_ILL_HANDLE));
    
  tsactive[handle] &= TS_START_TS;
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/************************************************/
TS_ErrorCode_t ts_elapsed (tstamp t1, float *time)
/************************************************/
{
  // Calculate the time elapsed between a time stamp and tzero in us
  float fdiff, low, hi;
  int diff;

  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_elapsed: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }

  if (!tst0)  // tzero has not yet been recorded
    return(RCC_ERROR_RETURN(0, TSE_NO_REF));

  diff = t1.low - tzero.low;
  fdiff = (float)diff;
  low = fdiff / frequency_low_us;

  diff = t1.high - tzero.high;
  fdiff = (float)diff;
  hi = fdiff / frequency_high_us;

  *time = hi + low;
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/*************************************/
float ts_duration(tstamp t1, tstamp t2)
/*************************************/
{
  // Calculate the time elapsed between two time stamps (for on-line processing)
  double time1, time2;
  int ret;
  
  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_duration: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }

  if (!got_freq)
  {
    ret = ts_get_freq();
    if(ret)
      return(RCC_ERROR_RETURN(ret, TSE_NO_FREQ));
  }

  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: frequency_high=" << frequency_high);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: frequency_low =" << frequency_low);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: t1.high       =" << t1.high);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: t1.low        =" << t1.low);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: t2.high       =" << t2.high);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: t2.low        =" << t2.low);
  time1 = (double)t1.high / frequency_high + (double)t1.low / frequency_low;
  time2 = (double)t2.high / frequency_high + (double)t2.low / frequency_low;
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: time1         =" << time1);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: time2         =" << time2);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_duration: duration      =" << time2 - time1);
  return (time2 - time1);
}


/**********************************/
int ts_compare(tstamp t1, tstamp t2) 
/**********************************/
{
  // Compare two timestamps
  // Returns:
  //  1 if t1>t2
  // -1 if t1<t2 
  //  0 if t1=t2

  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_compare: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }
    
  if (t1.high > t2.high) return 1;
  if (t1.high < t2.high) return -1;
  if (t1.low > t2.low) return 1;
  if (t1.low < t2.low) return -1;
  return 0;  
}


/***********************************************/
TS_ErrorCode_t ts_offset(tstamp *ts, u_int usecs)
/***********************************************/
{
  // Offset a given timestamp ts by a given time in usec  

  long long int th64, ticks, ts64;
  
  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_offset: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }

  ticks = (unsigned long long int)((double)usecs * frequency_low_us);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_offset: " << usecs << " us = " << ticks << " ticks");
  
  th64 = ts->high;
  ts64 = (th64 << 32) + ts->low;
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_offset: ts64 =" << ts64);
  ts64 += ticks;
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_offset: ts64 + ticks = " << ts64);

  ts->low = ts64 & 0xffffffff;
  ts->high = ts64 >> 32;
  
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_offset: New  low 32 bit of time stamp = " << HEX(ts->low));
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_offset: New high 32 bit of time stamp = " << HEX(ts->high));
  
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/********************************************************/
TS_ErrorCode_t ts_wait_until(tstamp target, u_int *nyield)
/********************************************************/
{
  // Yield the current thread until a target time
  int cnt;
  tstamp now;

  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_wait_until: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                           
  }
  
  cnt = 0;
  ts_clock(&now);
  while(ts_compare(target, now) > 0)
  {
    sched_yield();  //Let's give other processes a chance
    cnt++;
    ts_clock(&now);
  }
  *nyield = cnt;

  return(RCC_ERROR_RETURN(0, TSE_OK));  
}


/***********************************/
TS_ErrorCode_t ts_clock(tstamp *time)
/***********************************/
{
  // Record a time stamp for on-line processing
  u_int a, b;

  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_clock: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                  
  }

  RDTSC(b,a);

  time->high = a;
  time->low = b;
  return(RCC_ERROR_RETURN(0, TSE_OK));
}


/************************************************/
TS_ErrorCode_t ts_wait(u_int usecs, u_int *nyield)
/************************************************/
{
  volatile u_int a, b, y, z;
  int cnt = 0, longsleep;
  double delay, waited = 0, time1, time2;

  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_wait: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                           
  }
  
  //If we have to wait for more than 10 ms (1 Linux jiffie) we first use the Linux usleep call to 
  //go to a deep and refeshing sleep. This is to let the CPU cool down and save lots of energy
  //There is a slight risk of oversleeping
  if(usecs > 10000)
  {
    RDTSC(b,a);
    longsleep = (usecs / 10000) * 10000;
    DEBUG_TEXT(DFDB_RCCTS, 10, "ts_wait: longsleep=" << longsleep);
    usleep(longsleep);
    RDTSC(z,y);
    time1 = (double)a / frequency_high_us + (double)b / frequency_low_us;
    time2 = (double)y / frequency_high_us + (double)z / frequency_low_us;
    waited = time2 - time1;
    //Have we already oversleept?
    DEBUG_TEXT(DFDB_RCCTS, 10, "ts_wait: Back from usleep after " << waited << " us");    
    if(waited > (double)usecs)
      return(RCC_ERROR_RETURN(0, TSE_OK));  
  }
    
  //Now, for the rest of the night we check the time more frequently as we want to get up at the right moment
  delay = (double)usecs - waited;
  DEBUG_TEXT(DFDB_RCCTS, 10, "ts_wait: delay =" << delay);

  RDTSC(b,a);
  while(1)
  {
    RDTSC(z,y);
    time1 = (double)a / frequency_high_us + (double)b / frequency_low_us;
    time2 = (double)y / frequency_high_us + (double)z / frequency_low_us;
    waited = time2 - time1; 
    DEBUG_TEXT(DFDB_RCCTS, 20, "ts_wait: a=" << a << "     b=" << b);
    DEBUG_TEXT(DFDB_RCCTS, 20, "ts_wait: y=" << y << "     z=" << z);
    DEBUG_TEXT(DFDB_RCCTS, 20, "ts_wait: waited=" << waited);

    if(waited > delay)
      break;

    sched_yield();  //Let's give other processes a chance
    cnt++;
  }
  DEBUG_TEXT(DFDB_RCCTS, 6, "ts_wait: waited for " << waited << " us. cnt = " << cnt);
  
  *nyield = cnt;
  return(RCC_ERROR_RETURN(0, TSE_OK));  
}


/**********************************/
TS_ErrorCode_t ts_delay(u_int usecs)
/**********************************/
{
  volatile u_int a, b, y, z;
  double delay, waited, time1, time2;

  delay = (float)usecs;

  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_delay: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                           
  }

  RDTSC(b,a);

  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: delay=" << delay);

  do
  {
    RDTSC(z,y);

    time1 = (double)a / frequency_high_us + (double)b / frequency_low_us;
    time2 = (double)y / frequency_high_us + (double)z / frequency_low_us;
    waited = time2 - time1; 
    DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: a=" << a << "     b=" << b);
    DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: y=" << y << "     z=" << z);
    DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: waited=" << waited);
  }
  while(waited < delay);

  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: a=" << a << "     b=" << b);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: y=" << y << "     z=" << z);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: frequency_high_us: " << frequency_high_us);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: frequency_low_us:  " << frequency_low_us);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: frequency_high:    " << frequency_high);
  DEBUG_TEXT(DFDB_RCCTS, 20, "ts_delay: frequency_low:     " << frequency_low);

  return(RCC_ERROR_RETURN(0, TSE_OK));  
}


/***************************************************************************/
TS_ErrorCode_t packTS_err_get (err_pack err, err_str pid_str, err_str en_str)
/***************************************************************************/
{
  err_type result;

  strcpy(pid_str, P_ID_TS_STR);

  result = TSE_NOCODE;
  switch(err)
  {
    case TSE_OK:         strcpy(en_str, TSE_OK_STR);         break;
    case TSE_NO_FREQ:    strcpy(en_str, TSE_NO_FREQ_STR);    break;
    case TSE_FILE:       strcpy(en_str, TSE_FILE_STR);       break;
    case TSE_ILL_HANDLE: strcpy(en_str, TSE_ILL_HANDLE_STR); break;
    case TSE_ILL_SIZE:   strcpy(en_str, TSE_ILL_SIZE_STR);   break;
    case TSE_PFILE:      strcpy(en_str, TSE_PFILE_STR);      break;
    case TSE_NO_REF:     strcpy(en_str, TSE_NO_REF_STR);     break;
    case TSE_ERROR_FAIL: strcpy(en_str, TSE_ERROR_FAIL_STR); break;
    case TSE_IS_CLOSED:  strcpy(en_str, TSE_IS_CLOSED_STR);  break;
    case TSE_ILL_MODE:   strcpy(en_str, TSE_ILL_MODE_STR);   break;
    case TSE_NOCODE:     strcpy(en_str, TSE_NOCODE_STR);     break;
  } 

  if (result == TSE_NOCODE)
    strcpy(en_str, TSE_NOCODE_STR);

  return(RCC_ERROR_RETURN(0, result));
}


/*******************************************/
TS_ErrorCode_t ts_histo(tstamp t1, tstamp t2)
/*******************************************/
{
  // Calculate the time elapsed between two time stamps (for on-line processing)
  #define NIGNORE 100000
  #define HISTOBASE 1000
  #define HISTUPDATE 10000
  #define NHISTO 50  
  u_int loop;
  u_int64_t t1l, t2l;
  int histobin;
  static u_int64_t tlmin = 0, tlmax = 0, rawdata;
  static u_int underflow = 0, overflow = 0, dbgfuse = 0, binwidth, ignore = 0, nts = 0, first = 1, histo[NHISTO];
  FILE *outf1;
	    
  if (!isopen)
  {
    DEBUG_TEXT(DFDB_RCCTS, 5, "ts_histo: Error=library is closed");
    return(RCC_ERROR_RETURN(0, TSE_IS_CLOSED));                                
  }

  if (ignore++ < NIGNORE)
    return(TSE_OK);
    
  if (first)  //compute histogram limits
  {
    t1l = ((u_int64_t)t1.high << 32) + t1.low;
    t2l = ((u_int64_t)t2.high << 32) + t2.low;
    rawdata = t2l - t1l;

    if (nts++ == 0)         
    {
      tlmin = rawdata;
      tlmax = rawdata;
      DEBUG_TEXT(DFDB_RCCTS, 20, "ts_histo: FIRST: tlmin = " << tlmin << " tlmax = " << tlmax);
    }
    else
    {
      if (rawdata < tlmin)
	tlmin = rawdata;
      if (rawdata > tlmax)
	tlmax = rawdata;
      DEBUG_TEXT(DFDB_RCCTS, 20, "ts_histo: NEXT: rawdata = " << rawdata << " tlmin = " << tlmin << " tlmax = " << tlmax);
    }
    if (nts == HISTOBASE)
    {
      first = 0;
      nts = 0;
      binwidth = 1 + ((tlmax - tlmin) / NHISTO);
      DEBUG_TEXT(DFDB_RCCTS, 20, "ts_histo: LAST: tlmin = " << tlmin << " tlmax = " << tlmax);
      DEBUG_TEXT(DFDB_RCCTS, 20, "ts_histo: binwidth = " << binwidth);
      
      for(loop = 0; loop < NHISTO; loop++)
        histo[loop] = 0;
    }
  }
  else     //update histogram
  {
    t1l = ((u_int64_t)t1.high << 32) + t1.low;
    t2l = ((u_int64_t)t2.high << 32) + t2.low;
    rawdata = t2l - t1l;
    histobin = (rawdata - tlmin)  / binwidth;
    if (histobin < 0)
      underflow++;
    else if (histobin > (NHISTO - 1))
      overflow++;      
    else
      histo[histobin]++;
    
    if (dbgfuse++ < 100)
      DEBUG_TEXT(DFDB_RCCTS, 20, "ts_histo: rawdata = " << rawdata << " histobin = " << histobin);
    
    if(nts++ == HISTUPDATE)
    {
      DEBUG_TEXT(DFDB_RCCTS, 20, "ts_histo: (Re)opening file");
      outf1 = fopen("/tmp/ts_histo", "w+");
      if (outf1 == 0)
      {
	DEBUG_TEXT(DFDB_RCCTS, 5, "ts_histo: Can't open output file");
        return(RCC_ERROR_RETURN(0, TSE_PFILE));
      }
      
      fprintf(outf1, "Underflow: %d\n", underflow);
      for (loop = 0; loop < NHISTO; loop++)
      {
        fprintf(outf1, "%lu %d\n", tlmin + loop * binwidth, histo[loop]);
        DEBUG_TEXT(DFDB_RCCTS, 20, "ts_histo: bin = " << tlmin + loop * binwidth << " data = " << histo[loop]);
      }
      fprintf(outf1, "Overflow: %d\n", overflow);
      
      DEBUG_TEXT(DFDB_RCCTS, 20, "ts_histo: histogram updated");
      fclose(outf1);  
      nts = 0;
    }
  }
  return(TSE_OK);
}
























