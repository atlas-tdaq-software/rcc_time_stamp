#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/types.h>
#include "rcc_time_stamp/tstamp.h"

#define MAX_EL 5000

typedef struct
  {
  int num;
  int range;
  } T_histo;

int main(void)
{
int need,anum,num,dnum,first;
T_histo histo[20];
FILE *inf,*outf;
char inname[20], outname[20];
tstamp t1,t2,data[MAX_EL];
int dmin,dmax,delta[MAX_EL];

printf("Enter The name of the input file: ");
scanf("%s",inname);
printf("Enter The name of the output file: ");
scanf("%s",outname);

/*open files*/
inf=fopen(inname,"r");
if(inf==0)
  {
  printf("Can't open input file\n");
  exit(0);
  }
outf=fopen(outname,"w+");
if (outf==0)
  {
  printf("Can't open output file\n");
  exit(0);
  }

/*read data*/
num=0;
while(!feof(inf))
  {
  fscanf(inf,"%u",&data[num].high);
  fscanf(inf,"%u",&data[num].low);
  fscanf(inf,"%u",&data[num].data);
  num++;
  if (num==MAX_EL)
    break;
  }
num--;

/*close the input file*/
fclose(inf);

/*histogram a duration defined by two events*/
anum=0;
dnum=0;
dmax=0;
dmin=999999;
first=1;
need=2;  /*start event*/
while(anum<num)
  { 
  printf("first=%u, anum=%u\n",first,anum);
  if ((data[anum].data==need)) /*start event*/
    {
    if(first)
      {
      t1=data[anum];
      first=0;
      anum++;
      printf("first: t1.low=%u\n",t1.low);
      }
    else
      {
      if(anum==num)
        {
        printf("That was the last one  num=%u  anum=%u\n",num,anum);
        anum++;
        }
      else
        {
        t2=data[anum];
        delta[dnum]=t2.low-t1.low;
        if(delta[dnum]<dmin)
          dmin=delta[dnum]; 
        if(delta[dnum]>dmax)
          dmax=delta[dnum];
        printf("second: t2.low=%u  dnum=%u  dt=%u\n\n\n",t2.low,dnum,delta[dnum]);
        dnum++;
        first=1;
        }
      }
    }
  } 
printf("%u durations calculated\n",dnum);
printf("dmin=%u\n",dmin);
printf("dmax=%u\n",dmax);

/*initialise histogram*/
for(anum=0;anum<20;anum++)
 {
 histo[anum].range=anum;
 histo[anum].num=0;
 }

/*fill histogram*/
for(anum=0;anum<dnum;anum++)
  histo[delta[anum]].num++;

/*write histogram to output file*/
for(anum=0;anum<20;anum++)
  fprintf(outf,"%u %u\n",histo[anum].num,histo[anum].range);

/*close output file*/
fclose(outf);

return 0;

}
