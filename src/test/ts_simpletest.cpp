/************************************************************************/
/*                                                                      */
/*   file: tstamptest.c                                                 */
/*   author: Markus Joos, CERN-EP/ESS                                   */
/*                                                                      */
/*   This is a test program for the timestamp library.                  */
/*                                                                      */
/*   History:                                                           */
/*   13.Aug.98  MAJO  created                                           */
/*    1.Mar.99  MAJO  macros introduced                                 */
/*                                                                      */
/*******Copyright 1998 - The software with that certain something********/

#include <stdio.h>
#include <unistd.h>
#include "rcc_time_stamp/tstamp.h"
#include "rcc_time_stamp/my_ts_events.h"

int main(void)
{
  tstamp t1,t2;

  TS_OPEN(1,TS_H1);
  TS_CLOCK(&t1);

  sleep(100);
  TS_CLOCK(&t2);

  printf ("time is %f \n",ts_duration(t1, t2));

  TS_CLOSE(TS_H1);

  return 0;
}
