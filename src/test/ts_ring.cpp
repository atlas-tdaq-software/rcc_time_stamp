#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/types.h>
#include "rcc_time_stamp/tstamp.h"

int main(int argc, char *argv[])
{
  FILE *inf;
  char inname[20];
  float f_hi,f_lo;
  unsigned int h1, h2 = 0, l1, l2 = 0, line, dummy;

  if (argc != 2)
  {
    printf("use: %s <filename>\n", argv[0]);
    printf("  Read data from file <filename> and print the line number at which\n");
    printf("  the last event is stored\n");
    exit(0);
  }
  if ((argc == 2) && (sscanf(argv[1], "%s", inname) == 1)) {argc--;} else {exit(0);}

  // open file
  inf = fopen(inname, "r");
  if (inf == 0)
  {
    printf("Can't open input file\n");
    exit(0);
  }

  // read the frequencies
  fscanf(inf,"%e",&f_hi);
  fscanf(inf,"%e",&f_lo);

  // read data
  line = 0;
  while(!feof(inf))
  {
    fscanf(inf, "%u", &h1);
    fscanf(inf, "%u", &l1);
    fscanf(inf, "%u", &dummy);
    line++;
    if (line > 1)
    {
      if (h1 < h2)
      {
        printf("Line number = %d\n", line + 1);
        //printf("h1 = %u h2 = %u\n", h1, h2);
        break;
      }
      if (h1 == h2 && l1 < l2)
      {
        printf("Line number = %d\n", line + 1);
        // printf("h1 = %u h2 = %u l1 = %u l2 = %u\n", h1, h2, l1, l2);
        break;
      }      
    }
    h2 = h1;
    l2 = l1;
  }

  // close the input file
  fclose(inf);

  return 0;
}
