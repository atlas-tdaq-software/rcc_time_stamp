#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/types.h>
#include "rcc_time_stamp/tstamp.h"

int main(int argc, char *argv[])
{
  unsigned int num, adata, odata, dl, dh, mingap;
  unsigned long long atime, otime;
  char inname[20];
  double dummy;
  FILE *inf;
  
  if (argc < 3)
  {
    printf("use: %s <filename> <delta>\n",argv[0]);
    printf("  Read data from file <filename> and output time intervals above a certain value\n");
    printf("  \n");
    exit(0);
  }
  if ((argc==3) && (sscanf(argv[2], "%u",&mingap) == 1)) {argc--;} else {exit(0);}
  if ((argc==2) && (sscanf(argv[1], "%s",inname) == 1)) {argc--;} else {exit(0);}

  /*open files*/
  inf = fopen(inname,"r");
  if (inf == 0)
  {
    printf("Can't open input file\n");
    exit(0);
  }
  /*read the frequencies*/
  fscanf(inf, "%le", &dummy);
  fscanf(inf, "%le", &dummy);

  /*read data*/
  num = 2;
  fscanf(inf, "%u", &dh);
  fscanf(inf, "%u", &dl);
  fscanf(inf, "%u", &adata);
  atime = ((long long)dh << 32) | dl;  
  while(!feof(inf))
  {
    num++;
    odata = adata;
    otime = atime;
    fscanf(inf, "%u", &dh);
    fscanf(inf, "%u", &dl);
    fscanf(inf, "%u", &adata);
    atime = ((long long)dh << 32) | dl;
    if ((atime - otime) > mingap)
    {
      printf("Gap of %lld time stamps fond in line %d\n", (atime - otime), num);
      printf("high = %u  low = %u\n", (unsigned int)(otime >> 32), (unsigned int)(otime & 0xffffffff));
      printf("odata = %u  adata = %u\n", odata, adata);
    }  
  }
  num--;

  /*close the input file*/
  fclose(inf);

  exit(0);
}
