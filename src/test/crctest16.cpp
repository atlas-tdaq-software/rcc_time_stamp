#include <string.h>
#include <stdio.h>      
#include <stdlib.h>
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "rcc_error/rcc_error.h"

const int order = 16;
const unsigned long polynom = 0x1021;
const unsigned long crcinit = 0xffff;
const unsigned long crcxor = 0x0000;

// internal global values:
unsigned long crcmask;
unsigned long crchighbit;
unsigned long crcinit_direct;
unsigned long crcinit_nondirect;
unsigned long crctab16[65536];


// subroutines
void generate_crc_table16()   // make CRC lookup table used by table algorithms
{
  unsigned long bit, crc, i, j;

  for (i = 0; i < 65536; i++) 
  {
    crc = i;
    //crc = crc << (order - 16); //order - 16 = 0

    for (j = 0; j < 16; j++) 
    {
      bit = crc & crchighbit;
      crc <<= 1;
      if (bit) 
        crc ^= polynom;
    }			

    crc &= crcmask;
    crctab16[i] = crc;
    if (i < 300)
      printf("crctab16[%d] = 0x%08x\n", i, crctab16[i]); 
  }
}
		
unsigned long crctablefast16 (unsigned short* p, unsigned long len) 
{
  unsigned long crc = crcinit_direct;

  while (len--) 
    crc = (crc << 16) ^ crctab16[((crc >> (order - 16)) & 0xffff) ^ *p++];

  crc ^= crcxor;
  crc &= crcmask;
  return(crc);
}


unsigned long crctable16 (unsigned short* p, unsigned long len) 
{
  unsigned long crc = crcinit_nondirect;

  while (len--) 
    crc = ((crc << 16) | *p++) ^ crctab16[(crc >> (order - 16)) & 0xffff];

  while (++len < order / 16) 
    crc = (crc << 16) ^ crctab16[(crc >> (order - 16)) & 0xffff];

  crc ^= crcxor;
  crc &= crcmask;
  return(crc);
}


int main() 
{
  int ret, evsize = 1024 * 1024, i;
  unsigned char *event;
  unsigned short ev1[1000], *event16;
  unsigned long bit, crc, crc_result;
  float delta;
  tstamp ts1, ts2;


  // at first, compute constant bit masks for whole CRC and CRC high bit
  crcmask = ((((unsigned long) 1 << (order - 1)) - 1) << 1) | 1;
  crchighbit = (unsigned long) 1 << (order - 1);

  generate_crc_table16();   // generate lookup table

  // compute missing initial CRC value
  crcinit_direct = crcinit;
  crc = crcinit;
  for (i = 0; i < order; i++) 
  {
    bit = crc & 1;
    if (bit) 
      crc ^= polynom;
    crc >>= 1;
    if (bit) 
      crc |= crchighbit;
  }	
  crcinit_nondirect = crc;

  printf("\n");
  printf("Parameters:\n");
  printf(" polynom             :  0x%x\n", polynom);
  printf(" order               :  %d\n", order);
  printf(" crcinit             :  0x%x direct, 0x%x nondirect\n", crcinit_direct, crcinit_nondirect);
  printf(" crcxor              :  0x%x\n", crcxor);
  printf("\n");
  printf("Results:\n");

  ret = ts_open(1, TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);

  printf("Testing CRC value\n");
  ev1[0]  = 0xEE12;
  ev1[1]  = 0x0000;
  ev1[2]  = 0x0301;
  ev1[3]  = 0x5555;
  ev1[4]  = 0xAAAA;
  ev1[5]  = 0x0000;
  ev1[6]  = 0x5566;
  ev1[7]  = 0x0000;
  ev1[8]  = 0xCDCD;
  ev1[9]  = 0x0000;
  ev1[10] = 0xDD00;
  ev1[11] = 0xDD00;
  ev1[12] = 0xDD00;
  ev1[13] = 0xDD00;
  ev1[14] = 0xDD00;
  ev1[15] = 0xDD00;
  ev1[16] = 0xDD00;
  ev1[17] = 0xDD00;
  ev1[18] = 0xDD00;
  ev1[19] = 0xDD00;
  ev1[20] = 0xDD00;
  ev1[21] = 0xDD00;
  ev1[22] = 0xDD00;
  ev1[23] = 0xDD00;
  ev1[24] = 0xDD00;
  ev1[25] = 0xDD00;
  ev1[26] = 0xDD00;
  ev1[27] = 0xDD00;
  ev1[28] = 0xDD00;
  ev1[29] = 0xDD00;
  ev1[30] = 0xDD00;
  ev1[31] = 0xDD00;
  ev1[32] = 0xDD00;
  ev1[33] = 0xDD00;
  ev1[34] = 0xDD00;
  ev1[35] = 0xDD00;
  ev1[36] = 0xDD00;
  ev1[37] = 0xDD00;
  ev1[38] = 0xDD00;
  ev1[39] = 0xDD00;
  ev1[40] = 0xDD00;
  ev1[41] = 0xDD00;
  ev1[42] = 0x0000;
  ev1[43] = 0x0000;
  ev1[44] = 0x0000;
  printf("Event 1 MSW. Expected CRC = 0xb603/c06d:\n");

  ret = ts_clock(&ts1);
  crc_result = crctable16(ev1, 45);
  ret = ts_clock(&ts2);
  printf("Delay = %f us\n", 1000000.0 * ts_duration(ts1, ts2));
  printf("crc (table)      = 0x%x\n", crc_result);

  ret = ts_clock(&ts1);
  crc_result = crctablefast16(ev1, 45);
  ret = ts_clock(&ts2);
  printf("Delay = %f us\n", 1000000.0 * ts_duration(ts1, ts2));
  printf("crc (table fast) = 0x%x\n", crc_result);

  ev1[0]  = 0x34EE;
  ev1[1]  = 0x0009;
  ev1[2]  = 0x00AA;
  ev1[3]  = 0x5555;
  ev1[4]  = 0xAAAA;
  ev1[5]  = 0x0000;
  ev1[6]  = 0x7788;
  ev1[7]  = 0x0007;
  ev1[8]  = 0xCDCD;
  ev1[9]  = 0x0000;
  ev1[10] = 0x0000;
  ev1[11] = 0x0001;
  ev1[12] = 0x0002;
  ev1[13] = 0x0003;
  ev1[14] = 0x0004;
  ev1[15] = 0x0005;
  ev1[16] = 0x0006;
  ev1[17] = 0x0007;
  ev1[18] = 0x0008;
  ev1[19] = 0x0009;
  ev1[20] = 0x000A;
  ev1[21] = 0x000B;
  ev1[22] = 0x000C;
  ev1[23] = 0x000D;
  ev1[24] = 0x000E;
  ev1[25] = 0x000F;
  ev1[26] = 0x0010;
  ev1[27] = 0x0011;
  ev1[28] = 0x0012;
  ev1[29] = 0x0013;
  ev1[30] = 0x0014;
  ev1[31] = 0x0015;
  ev1[32] = 0x0016;
  ev1[33] = 0x0017;
  ev1[34] = 0x0018;
  ev1[35] = 0x0019;
  ev1[36] = 0x001A;
  ev1[37] = 0x001B;
  ev1[38] = 0x001C;
  ev1[39] = 0x001D;
  ev1[40] = 0x001E;
  ev1[41] = 0x001F;
  ev1[42] = 0x0001;
  ev1[43] = 0x0020;
  ev1[44] = 0x0000;
  printf("Event 1 LSW. Expected CRC = 0xb263/c64d:\n");
  
  ret = ts_clock(&ts1);
  crc_result = crctable16(ev1, 45);
  ret = ts_clock(&ts2);
  printf("Delay = %f us\n", 1000000.0 * ts_duration(ts1, ts2));
  printf("crc (table)      = 0x%x\n", crc_result);

  ret = ts_clock(&ts1);
  crc_result = crctablefast16(ev1, 45);
  ret = ts_clock(&ts2);
  printf("Delay = %f us\n", 1000000.0 * ts_duration(ts1, ts2));
  printf("crc (table fast) = 0x%x\n", crc_result);

  ev1[0]  = 0xEE12; 
  ev1[1]  = 0x0000;
  ev1[2]  = 0x0301;
  ev1[3]  = 0x5555;
  ev1[4]  = 0xAAAA;
  ev1[5]  = 0x8888;
  ev1[6]  = 0x5566;
  ev1[7]  = 0x0000;
  ev1[8]  = 0xCDCD;
  ev1[9]  = 0x0000;
  ev1[10] = 0xDD00;
  ev1[11] = 0xDD00;
  ev1[12] = 0xDD00;
  ev1[13] = 0xDD00;
  ev1[14] = 0xDD00;
  ev1[15] = 0xDD00;
  ev1[16] = 0xDD00;
  ev1[17] = 0xDD00;
  ev1[18] = 0xDD00;
  ev1[19] = 0xDD00;
  ev1[20] = 0xDD00;
  ev1[21] = 0xDD00;
  ev1[22] = 0xDD00;
  ev1[23] = 0xDD00;
  ev1[24] = 0xDD00;
  ev1[25] = 0xDD00;
  ev1[26] = 0xDD00;
  ev1[27] = 0xDD00;
  ev1[28] = 0xDD00;
  ev1[29] = 0xDD00;
  ev1[30] = 0xDD00;
  ev1[31] = 0xDD00;
  ev1[32] = 0xDD00;
  ev1[33] = 0xDD00;
  ev1[34] = 0xDD00;
  ev1[35] = 0xDD00;
  ev1[36] = 0xDD00;
  ev1[37] = 0xDD00;
  ev1[38] = 0xDD00;
  ev1[39] = 0xDD00;
  ev1[40] = 0xDD00;
  ev1[41] = 0xDD00;
  ev1[42] = 0x0000;
  ev1[43] = 0x0000;
  ev1[44] = 0x0000;
  printf("Event 2 MSW. Expected CRC = 0xfe82/417f:\n");

  ret = ts_clock(&ts1);
  crc_result = crctable16(ev1, 45);
  ret = ts_clock(&ts2);
  printf("Delay = %f us\n", 1000000.0 * ts_duration(ts1, ts2));
  printf("crc (table)      = 0x%x\n", crc_result);

  ret = ts_clock(&ts1);
  crc_result = crctablefast16(ev1, 45);
  ret = ts_clock(&ts2);
  printf("Delay = %f us\n", 1000000.0 * ts_duration(ts1, ts2));
  printf("crc (table fast) = 0x%x\n", crc_result);

  ev1[0]  = 0x34EE; 
  ev1[1]  = 0x0009;
  ev1[2]  = 0x00AA;
  ev1[3]  = 0x5555;
  ev1[4]  = 0xAAAA;
  ev1[5]  = 0x8888;
  ev1[6]  = 0x7788;
  ev1[7]  = 0x0007;
  ev1[8]  = 0xCDCD;
  ev1[9]  = 0x0000;
  ev1[10] = 0x0000;
  ev1[11] = 0x0001;
  ev1[12] = 0x0002;
  ev1[13] = 0x0003;
  ev1[14] = 0x0004;
  ev1[15] = 0x0005;
  ev1[16] = 0x0006;
  ev1[17] = 0x0007;
  ev1[18] = 0x0008;
  ev1[19] = 0x0009;
  ev1[20] = 0x000A;
  ev1[21] = 0x000B;
  ev1[22] = 0x000C;
  ev1[23] = 0x000D;
  ev1[24] = 0x000E;
  ev1[25] = 0x000F;
  ev1[26] = 0x0010;
  ev1[27] = 0x0011;
  ev1[28] = 0x0012;
  ev1[29] = 0x0013;
  ev1[30] = 0x0014;
  ev1[31] = 0x0015;
  ev1[32] = 0x0016;
  ev1[33] = 0x0017;
  ev1[34] = 0x0018;
  ev1[35] = 0x0019;
  ev1[36] = 0x001A;
  ev1[37] = 0x001B;
  ev1[38] = 0x001C;
  ev1[39] = 0x001D;
  ev1[40] = 0x001E;
  ev1[41] = 0x001F;
  ev1[42] = 0x0001;
  ev1[43] = 0x0020;
  ev1[44] = 0x0000;
  printf("Event 2 LSW. Expected CRC = 0xfae2/475f:\n");

  ret = ts_clock(&ts1);
  crc_result = crctable16(ev1, 45);
  ret = ts_clock(&ts2);
  printf("Delay = %f us\n", 1000000.0 * ts_duration(ts1, ts2));
  printf("crc (table)      = 0x%x\n", crc_result);

  ret = ts_clock(&ts1);
  crc_result = crctablefast16(ev1, 45);
  ret = ts_clock(&ts2);
  printf("Delay = %f us\n", 1000000.0 * ts_duration(ts1, ts2));
  printf("crc (table fast) = 0x%x\n", crc_result);

exit(0);

  printf("Testing CRC speed\n");
  event16 = (unsigned short *) malloc(evsize);

  //Create some dummy data
  for (i = 0; i < evsize; i++)
    event16[i] = i & 0xff;
  crc_result = crctable16(event16, 10240);
  crc_result = crctablefast16(event16, 10240);

  printf("\nEvent size = 1 kbyte\n");
  ret = ts_clock(&ts1);
  crc_result = crctable16(event16, 512);
  ret = ts_clock(&ts2);
  delta = 1000000.0 * ts_duration(ts1, ts2);
  printf("crc (table16)      = 0x%x  Execution delay: %f us\n", crc_result, delta);
    
  ret = ts_clock(&ts1);
  crc_result = crctablefast16(event16, 512);
  ret = ts_clock(&ts2);
  delta = 1000000.0 * ts_duration(ts1, ts2);
  printf("crc (table fast16) = 0x%x  Execution delay: %f us\n", crc_result, delta);

  return(0);
}

