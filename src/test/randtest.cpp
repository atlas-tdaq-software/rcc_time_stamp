#include <iostream>
#include <stdlib.h>
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "rcc_error/rcc_error.h"

#define DSIZE 500000 

int main(void)
{
  int next, ret, nroi, loop, nl2a = 0, l1id, rol, nrols;
  int noDeleteGroup = 0;
  int deleteGrouping = 100;
  int  roi[8], roihist[9];
  float delta;
  double roip[8];
  int roip_i[8];
  double l2ap;
  int l2ap_i;
  int alea[DSIZE];
  tstamp ts1,ts2;

  ret = ts_open(1,TS_DUMMY);

  printf("Enter the seed: ");
  int seed = getdecd(0);
  srand(seed);

  printf("Enter dsize: ");
  int dsize = getdecd(DSIZE);
  
  int check = 0;
  for(loop=0;loop<dsize;loop++)
    if (drand48() <= 0.05) check++;  
  printf("drand48: %d below 5%%\n", check); 
  check = 0;
  for(loop=0;loop<dsize;loop++)
    if ( ((float)rand()/(float)RAND_MAX) <= 0.05) check++;  
  printf("rand: %d below 5%%\n", check); 

  printf("Enter the number of ROLs (1..8): ");
  nrols = getdecd(8);;

  for(loop = 0; loop < nrols; loop++)
  {
    printf("Enter the RoI percentage for ROL %d: ", loop);
    roip[loop] = (double)getdecd(5); 
    roip_i[loop] = (int) ((roip[loop]  / 100.0) * (double)RAND_MAX);
    std::cout << " loop = " << loop << " roip_i[loop] = " << roip_i[loop] << std::endl;
    roihist[loop] = 0;
    roi[loop] = 0; 
  }
  roihist[nrols] = 0;
  roi[nrols] = 0; 

  printf("Enter the L2A percentage: ");
  l2ap = (double)getdecd(2);
  l2ap_i = (int) ((l2ap  / 100.0) * (double)RAND_MAX);
  std::cout << " l2ap_i = " << l2ap_i << std::endl;
 
  for(loop=0;loop<dsize;loop++)
  {
    alea[loop] = rand();
  }

  ret = ts_clock(&ts1);
  next = 0;
  for(l1id = 0; l1id < 1000000; l1id++)
  {
    nroi = 0;
    for(rol = 0; rol < nrols; rol++)
    {
      if (alea[next] <= roip_i[rol])
      {
        roi[rol]++;
        nroi++;
      }
      if (++next == dsize) next = 0;
    }
    roihist[nroi]++;

    if (alea[next] <= l2ap_i)
      nl2a++;
    if (++next == dsize) next = 0;

    if (l1id % deleteGrouping)
    {
      noDeleteGroup++;
    }

  }
  ret = ts_clock(&ts2);

  printf("\n\nOne million L1IDs generated\n");
  printf("=======================================\n");
  for(rol = 0; rol < (nrols + 1); rol++)
    printf("Number of events with %d RoI request: %d\n", rol, roihist[rol]);
  for(rol = 0; rol < nrols; rol++)
    printf("Number of RoI requests for ROL %d: %d\n", rol, roi[rol]);
  printf("Number of events with L2A: %d\n", nl2a);
  printf(" Number of deleteGroup: %d\n",noDeleteGroup);
  
  delta = ts_duration(ts1,ts2);
  printf("Time spent in trigger generation loop = %f s\n", delta);
  printf("\n\n\n");
  
  ret = ts_close(TS_DUMMY);
}
