#include <stdlib.h>
#include <vector>
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "rcc_error/rcc_error.h"

#define DSIZE 78987

int main(void)
{
  int next, ret, nroi, loop, nl2a = 0,ndeletes =0, l1id, rol, roi[8] ; 
  int nrols, roip[8], l2ap, roihist[9];
  float delta, alea[100000];
  tstamp ts1,ts2;
  unsigned int numberOfRols,deleteGrouping;
  unsigned int l1Range = 1000000000 ;
  unsigned int level1Id = 0;
  unsigned int level1IdIncrement = 0;
  unsigned int lastReleased=0;
  unsigned int numberOfRolsInRoi=0;
  unsigned int firstRolInRos=0;
  unsigned int lastRolInRos;
  unsigned int l2RequestFraction;
  unsigned int ebRequestFraction;
  unsigned int ebRequestGap= 100000;

  printf("Enter the number of ROLs (1..8): ");
  numberOfRols = getdecd(8) ;
  lastRolInRos = numberOfRols - 1 ;

  printf("Enter the RoI percentage: ");
  l2RequestFraction = getdecd(5); 
  for(loop = 0; loop < numberOfRols; loop++)
  {
    roihist[loop] = 0;
    roi[loop] = 0; 
  }
  roihist[numberOfRols] = 0;
  roi[numberOfRols] = 0; 

  printf("Enter the L2A percentage: ");
  ebRequestFraction = getdecd(2);
  ret = ts_open(1,TS_DUMMY);

  printf("Enter the Delete grouping: ");
  deleteGrouping = getdecd(100);

  vector<int> numberOfRolsInRoiVector ;
  vector<int> l2RequestGapVector;
  if (l2RequestFraction>0 && l2RequestFraction<= 100 && numberOfRols>0) {
    //Probability of having a L2Request on a Robin
    float p = l2RequestFraction/100. ;
    //Calculate global probability of NOT having a L2Request
    //on the full ROS
    float p0 = 1. ;
    for (unsigned int i=0; i<numberOfRols; i++) {
      p0 *= (1.-p) ;
    }
    //Mean interval between L2Requests
    float meanL2RequestGap = 1./(1.-p0) ;
    //Generate a number of possible integer gaps that mediated
    //give the above mean interval and fill a vector of gaps
    unsigned int minL2RequestGap = (int)meanL2RequestGap;
    unsigned int maxL2RequestGap = minL2RequestGap+1; 
    float c1 = meanL2RequestGap-minL2RequestGap;
    float c2 = maxL2RequestGap-meanL2RequestGap;     
    if (c1>c2) {
      int f = (int)(c1/c2) ;
      for (int i=0; i<f; i++) {
	l2RequestGapVector.push_back(maxL2RequestGap);
      }
      l2RequestGapVector.push_back(minL2RequestGap);
    }
    else {
      int f = (int)(c2/c1) ;
      for (int i=0; i<f; i++) {
	l2RequestGapVector.push_back(minL2RequestGap);
      }
      l2RequestGapVector.push_back(maxL2RequestGap);       
    }
    //Now calculate a vector with the distribution of Rols
    //per RoI
    float pi = p0;
    for (unsigned int i=1; i<=numberOfRols; i++) {
      pi = pi * (p/(1.-p)) * (numberOfRols-i+1) / i ; 
      for (int j=0; j<(100000*pi); j++) {
	numberOfRolsInRoiVector.push_back(i) ;
      }
    }     
  }
  else {
    l2RequestGapVector.push_back(1000000) ;
    numberOfRolsInRoiVector.push_back(1) ;
  }
  unsigned int numberOfGapsVectorElements = l2RequestGapVector.size() ;
  unsigned int numberOfRolsVectorElements = numberOfRolsInRoiVector.size() ;
    
  if (ebRequestFraction>0) {
    ebRequestGap=100/ebRequestFraction;
  }
  
  cout << "EmulatedTriggerIn: Generating L2Request fraction "
       << l2RequestFraction  
       << "% per Robin (" << numberOfRols 
       << "), EBRequest fraction " << ebRequestFraction
       << "%, deletes grouped in "
       << deleteGrouping << "s"
       << endl ;  

  unsigned int nextL2Request=l2RequestGapVector[0];
  int gapIndex = 0 ;
  unsigned int nextEBRequest=ebRequestGap;
  unsigned int nextRelease=deleteGrouping;
  
  unsigned int numberOfRequests = 0;
  ret = ts_clock(&ts1);
  while (numberOfRequests < 1000000) {
    
    level1IdIncrement=nextL2Request;
    if (nextEBRequest < level1IdIncrement) {
      level1IdIncrement=nextEBRequest;
    }
    if (nextRelease < level1IdIncrement) {
      level1IdIncrement=nextRelease;
    }
    nextEBRequest -= level1IdIncrement;
    nextRelease -= level1IdIncrement;
    nextL2Request -= level1IdIncrement;
    
    roihist[0] += level1IdIncrement;	    
    numberOfRequests += level1IdIncrement;

    level1Id = (level1Id+level1IdIncrement)%l1Range; //just to be sure
    
    if (nextL2Request==0) {
      roihist[0] -= 1 ;  //This is a L2 request! 
            
      if (numberOfRolsVectorElements>0) {
	numberOfRolsInRoi=numberOfRolsInRoiVector[rand()%numberOfRolsVectorElements];
      }
      roihist[numberOfRolsInRoi]++ ;	    
      unsigned int rolId=firstRolInRos;
      if (lastRolInRos>firstRolInRos) {
	rolId=firstRolInRos+rand()%(lastRolInRos-firstRolInRos+1);
      }
      for (unsigned int count=0; count<numberOfRolsInRoi; count++) {
	roi[rolId] ++ ;
	rolId++ ;
	if (rolId > lastRolInRos) {
	  rolId=firstRolInRos;
	}
      }		
      gapIndex = (gapIndex+1)%numberOfGapsVectorElements;
      nextL2Request = l2RequestGapVector[gapIndex];
    }
    
    if (nextEBRequest == 0) {      
      //EB Request
      nl2a++;
      nextEBRequest = ebRequestGap;
    }
    
    if (nextRelease == 0) {
      // Delete Request
      ndeletes++;
      nextRelease=deleteGrouping;
    }
  }  
 
  ret = ts_clock(&ts2);

  printf("\n\n%d L1IDs generated\n",numberOfRequests);
  printf("=======================================\n");
  for(rol = 0; rol < (numberOfRols + 1); rol++)
    printf("Number of events with %d RoI request: %d\n", rol, roihist[rol]);
  for(rol = 0; rol < numberOfRols; rol++)
    printf("Number of RoI requests for ROL %d: %d\n", rol, roi[rol]);
  printf("Number of events with L2A: %d\n", nl2a);
  printf("Number of delete requests: %d\n", ndeletes);
  
  delta = ts_duration(ts1,ts2);
  printf("Time spent in trigger generation loop = %f s\n", delta);
  printf("\n\n\n");
  
  ret = ts_close(TS_DUMMY);
}

