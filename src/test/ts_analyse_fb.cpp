#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/types.h>
#include "rcc_time_stamp/tstamp.h"

#define MAX_EL    10000
#define NBINS 22

typedef struct
  {
  unsigned int num;
  float range;
  } T_histo;

int main(int argc, char *argv[])
{
unsigned int starte, stope, need;
int anum,num,dnum,first;
T_histo histo[NBINS];
FILE *inf,*outf1,*outf2;
char inname[20], outname1[25], outname2[25];
tstamp t1 = {0, 0, 0}, t2,data[MAX_EL];
int dmin,dmax,delta[MAX_EL];
double tic_to_us,fddelta,fmin,fmax,fdelta[MAX_EL];
float f_lo,f_hi,h_min,h_max;

if (argc<4)
  {
  printf("use: %s <filename><start_event><stop_event>[<h_min><h_max>]\n",argv[0]);
  printf("     Read data from file <filename> and histogram the\n");
  printf("     delta-t distribution of the time interval defined\n");
  printf("     by <start_event> and <stop_event>\n");
  printf("     The optional parameters h_min and h_max are the\n");
  printf("     boundaries of the range to be histogrammed\n");
  printf("     The histogram allways has 22 bins. The bins #1 and #22 are the\n");
  printf("     under flow / over flow bins. If a values was specified\n");
  printf("     for h_min then bin #1 will contain all samples that are smaller\n");
  printf("     than h_min. The same applies for bin #22 with respect to h_max\n");
  printf("     Formats:\n");
  printf("     filename               => string\n");
  printf("     start_event,stop_event => unsigned int\n");
  printf("     h_min,h_max            => exp (e.g. 0.123456e+01)\n");
  exit(0);
  }
if ((argc==6)&&(sscanf(argv[5],"%e", &h_max)==1)) {argc--;} else {h_max=-1.0;}
if ((argc==5)&&(sscanf(argv[4],"%e", &h_min)==1)) {argc--;} else {h_min=-1.0;}
if ((argc==4)&&(sscanf(argv[3],"%u", &stope)==1)) {argc--;} else {exit(0);}
if ((argc==3)&&(sscanf(argv[2],"%u", &starte)==1)) {argc--;} else {exit(0);}
if ((argc==2)&&(sscanf(argv[1],"%s", inname)==1)) {argc--;} else {exit(0);}

sprintf(outname1,"%s_dt",inname);
sprintf(outname2,"%s_hist",inname);
printf("Raw delta t values will be written to file <%s>\n",outname1);
printf("Histogram data will be witten to file <%s>\n",outname2);

/*open files*/
inf=fopen(inname,"r");
if(inf==0)
  {
  printf("Can't open input file\n");
  exit(0);
  }
outf1=fopen(outname1,"w+");
if (outf1==0)
  {
  printf("Can't open output file 1\n");
  exit(0);
  }
outf2=fopen(outname2,"w+");
if (outf2==0)
  {
  printf("Can't open output file 2\n");
  exit(0);
  }

/*read the frequencies*/
fscanf(inf,"%e",&f_hi);
fscanf(inf,"%e",&f_lo);
printf("Frequency of low counter  = %e Hz\n",f_lo);
printf("Frequency of high counter = %e Hz\n",f_hi);

/*compute some constant(s)*/
tic_to_us=1000000.0/f_lo;

/*read data*/
num=0;
while(!feof(inf))
  {
  fscanf(inf,"%u",&data[num].high);
  fscanf(inf,"%u",&data[num].low);
  fscanf(inf,"%u",&data[num].data);
  num++;
  if (num==MAX_EL)
    break;
  }
num--;

/*close the input file*/
fclose(inf);


anum=0;
dnum=0;
dmax=0;
dmin=999999;
if(starte==stope)
  {
  /*histogram a duration defined by one event*/
  first=1;
  while(anum<num)
    { 
    if ((data[anum].data == starte))
      {
      if(first)
        {
        t1=data[anum];
        first=0;
        anum++;
        }
      else
        {
        if(anum==num)
          anum++;
        else
          {
          t2=data[anum];
          delta[dnum]=t2.low-t1.low;
          if(delta[dnum]<dmin) dmin=delta[dnum]; 
          if(delta[dnum]>dmax) dmax=delta[dnum];
          dnum++;
          first=1;
          }
        }
      }
    else
      anum++;
    } 
  }
else
  {
  /*histogram a duration defined by two events*/
  need=starte;  
  while(anum<=num)
    {
    if ((data[anum].data==need) && (need==starte)) /*start event*/
      {
      t1=data[anum];
      need=stope;
      }
    if ((data[anum].data==need) && (need==stope)) /*stop event*/
      {
      t2=data[anum];
      need=starte;
      delta[dnum] = t2.low - t1.low;
      if(delta[dnum]<dmin) dmin=delta[dnum]; 
      if(delta[dnum]>dmax) dmax=delta[dnum];
      dnum++;
      }
    anum++;
    }
  }
printf("%u durations calculated\n",dnum);
printf("dmin=%u clock tics\n",dmin);
printf("dmax=%u clock tics\n",dmax);

/*write raw delta-t file (unit = clock tics)*/
for(anum=0;anum<dnum;anum++)
  fprintf(outf1,"%u\n",delta[anum]);
fclose(outf1);

/*convert delta-t values from clock tics to useconds*/
for(anum=0;anum<dnum;anum++)
  fdelta[anum]=(float)delta[anum]*tic_to_us;
fmin=(float)dmin*tic_to_us;
fmax=(float)dmax*tic_to_us;

if(h_min!=-1.0)
  fmin=h_min;
if(h_max!=-1.0)
  fmax=h_max;

if(fmin>=fmax)
  {
  printf("ERROR: fmin>=fmax\n");
  exit(0);
  }

fddelta=(fmax-fmin)/(NBINS-2);

/*initialise histogram*/
for(anum=1;anum<(NBINS-1);anum++)
 {
 histo[anum].range=fmin+((anum-1)*fddelta);
 histo[anum].num=0;
 }
histo[0].num=0;  
histo[(NBINS-1)].num=0;


/*fill histogram*/
for(anum=0;anum<dnum;anum++)
  {
  if(fdelta[anum]==fmax)       histo[(NBINS-2)].num++;
  else if(fdelta[anum]>fmax)   histo[(NBINS-1)].num++;
  else if(fdelta[anum]<fmin)   histo[0].num++;
  else                         histo[1+(int)((fdelta[anum]-fmin)/fddelta)].num++;
  }

/*write histogram to output file*/
for(anum=0;anum<NBINS;anum++)
  {
  if(anum==0)
    fprintf(outf2,"%u under flow\n",histo[anum].num);
  else if(anum==21)
    fprintf(outf2,"%u over flow\n",histo[anum].num);
  else
    fprintf(outf2,"%u %e\n",histo[anum].num,histo[anum].range);
  }
fclose(outf2);

return 0;

}
