
/************************************************************************/
/*                                                                      */
/*   file: tstamptest.c                                                 */
/*   author: Markus Joos, CERN-EP/ESS                                   */
/*                                                                      */
/*   This is is a test program for the timestamp library.               */
/*                                                                      */
/*   History:                                                           */
/*   13.Aug.98  MAJO  created                                           */
/*    1.Mar.99  MAJO  macros introduced                                 */
/*                                                                      */
/*******Copyright 2003 - The software with that certain something********/

#include <stdlib.h>
#include <sched.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <math.h>
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "rcc_time_stamp/my_ts_events.h"


//Prototypes
int prim(u_int num);


/************/
int main(void)
/************/
{
  int mode, loop, ret;
  u_int yield, ncpus, cnt, pa, pb, pc, pd, dblevel = 20, dbpackage = DFDB_RCCTS;
  u_int isprim, num, sta = 10000001;
  tstamp ts1, ts2, ts3, ts4;
  float delta, dmax, dmin;
  cpu_set_t cpu[32], mask;

  printf("1  = Test functions\n");
  printf("2  = Test macros\n");
  printf("3  = Test ring buffer mode\n");
  printf("4  = Benchmark overhead of macros\n");
  printf("5  = Benchmark overhead of functions\n");
  printf("6  = Test SMP issues\n");
  printf("7  = Test waiting\n");
  printf("8  = SMP test 2\n");
  printf("9  = CPU benchmark\n");
  printf("10 = Test automatic histogram\n");
  printf("Enter mode : ");
  mode = getdecd(9);

  if (mode != 6 && mode != 9)
  {
    printf("Enter debug level (0..20) : ");
    dblevel = getdecd(0);
    DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  }
  
  if (mode == 1)
  {
    ret = ts_open(100, TS_H1);
    if (ret) rcc_error_print(stdout, ret);
    ret = ts_open(100, TS_H2);
    if (ret) rcc_error_print(stdout, ret);

    ret = ts_start(TS_H1);
    if (ret) rcc_error_print(stdout, ret);
    ret = ts_start(TS_H2);
    if (ret) rcc_error_print(stdout, ret);

    ret = ts_record(TS_H1, F1_START);
    if (ret) rcc_error_print(stdout, ret);
    ret = ts_record(TS_H2, F2_START);
    if (ret) rcc_error_print(stdout, ret);

    for(loop = 0; loop < 110; loop++)
    {
      printf("%d ", loop);
      ret = ts_record(TS_H1, F1_PRINT);
      if (ret) rcc_error_print(stdout, ret);

      if (loop == 50)
      {
        ret = ts_record(TS_H2, F2_PAUSE);
        if (ret) rcc_error_print(stdout, ret);
        ret = ts_pause(TS_H2);
        if (ret) rcc_error_print(stdout, ret);
      }
      if (loop == 80)
      {
        ret = ts_resume(TS_H2);
        if (ret) rcc_error_print(stdout, ret);
        ret = ts_record(TS_H2, F2_RESUME);
        if (ret) rcc_error_print(stdout, ret);
      }
      printf("0x%08x\n", loop);
      ret = ts_record(TS_H2, F2_PRINT);
      if (ret) rcc_error_print(stdout, ret);
    }
    ret = ts_record(TS_H1, F1_END);
    if (ret) rcc_error_print(stdout, ret);
    ret = ts_record(TS_H2, F2_END);
    if (ret) rcc_error_print(stdout, ret);

    ret = ts_save(TS_H1, (char *)"file1");
    if (ret) rcc_error_print(stdout, ret);
    ret = ts_save(TS_H2, (char *)"file2");
    if (ret) rcc_error_print(stdout, ret);

    ret = ts_close(TS_H1);
    if (ret) rcc_error_print(stdout, ret);
    ret = ts_close(TS_H2);
    if (ret) rcc_error_print(stdout, ret);
  }

  if (mode == 2)
  {
    TS_OPEN(100, TS_H1);
    TS_OPEN(100, TS_H2);

    TS_START(TS_H1);
    TS_START(TS_H2);

    TS_RECORD(TS_H1, F1_START);
    TS_RECORD(TS_H2, F2_START);

    for(loop = 0; loop < 110; loop++)
    {
      printf("%d ", loop);
      TS_RECORD(TS_H1, F1_PRINT);

      if (loop == 50)
      {
        TS_RECORD(TS_H2, F2_PAUSE);
        TS_PAUSE(TS_H2);
      }
      if (loop == 80)
      {
        TS_RESUME(TS_H2);
        TS_RECORD(TS_H2, F2_RESUME);
      }
      printf("0x%08x\n",loop);
      TS_RECORD(TS_H2, F2_PRINT);
    }
    TS_RECORD(TS_H1, F1_END);
    TS_RECORD(TS_H2, F2_END);

    TS_SAVE(TS_H1, "file1");
    TS_SAVE(TS_H2, "file2");

    TS_CLOSE(TS_H1);
    TS_CLOSE(TS_H2);
  }

  if (mode == 3)
  {    
    TS_OPEN(100, TS_H1);
    TS_MODE(TS_H1, TS_MODE_RING);
    TS_START(TS_H1);
    TS_RECORD(TS_H1, 11111111);

    for(loop = 0; loop < 123; loop++)
      TS_RECORD(TS_H1, loop);
      
    TS_RECORD(TS_H1, 22222222);
    TS_SAVE(TS_H1, "ring");
    TS_CLOSE(TS_H1);
  }


  if (mode == 4)
  {
    printf("Measuring overhead of TS_RECORD\n");
    TS_OPEN(10000, TS_H1);
    TS_START(TS_H1);
    for(loop = 0; loop < 10000; loop++)
      TS_RECORD(TS_H1, F1_OVERHEAD);
    TS_SAVE(TS_H1,"bench");
    TS_CLOSE(TS_H1); 
  }
  
  if (mode == 5)
  {    
    printf("Measuring overhead of ts_clock\n");
    ts_open(1, TS_DUMMY);
    ts_clock(&ts1);
    ts_clock(&ts2);
    ts_clock(&ts3);
    ts_clock(&ts4);
    delta = ts_duration(ts1, ts2); 
    printf(" delta 1 = %15.9f seconds (%6.1f ns)\n", delta, delta * 1000000000.0);  
    delta = ts_duration(ts2, ts3); 
    printf(" delta 2 = %15.9f seconds (%6.1f ns)\n", delta, delta * 1000000000.0); 
    delta = ts_duration(ts3, ts4); 
    printf(" delta 3 = %15.9f seconds (%6.1f ns)\n", delta, delta * 1000000000.0); 
    ts_close(TS_DUMMY);
  }
  
  if (mode == 6)
  {
#ifdef __x86_64__
    printf("This function is not supported on 64-bit platforms because of a porting issue with an assembler instruction\n");
#endif

#ifndef __x86_64__
    ret = sched_getaffinity(0, sizeof(cpu_set_t), &mask);
    if (ret)
    {
      printf("sched_getaffinity has returned %d\n", ret);
      exit(-1);
    }

    ncpus = 0;
    for (loop = 0; loop < 32; loop++)
    {
      ret = CPU_ISSET(loop, &mask);
      printf("CPU_ISSET returns %d for CPU %d\n", ret, loop);
    
      if(ret)
      {
        CPU_ZERO(&cpu[loop]); 
        CPU_SET(loop, &cpu[loop]);
	ncpus++;
      }      
    }
    
    printf("%d CPUs found\n", ncpus);

    cnt = 1000;

    ret = ts_open(cnt, TS_H1);
    if (ret) 
      rcc_error_print(stdout, ret);

    ret = ts_start(TS_H1);
    if (ret) 
      rcc_error_print(stdout, ret);
      
    while (cnt) 
    {
#ifdef __x86_64__
      __asm__ volatile ("push %%ebx; cpuid; mov %%ebx, %%edi; pop %%ebx" : "=a" (pa), "=D" (pb), "=c" (pc), "=d" (pd) : "a" (1)); 
#endif
      //printf("APIC ID = %d\n", pb >> 24);
      
      ret = ts_record(TS_H1, pb >> 24);
      if (ret) 
        rcc_error_print(stdout, ret);    
      
      //printf("Switching to CPU mask %d...\n", (cnt % ncpus));
      ret = sched_setaffinity(0, sizeof(cpu_set_t), &cpu[(cnt % ncpus)]);
      if (ret)
      {
	printf("errno = %d\n", errno);
	printf("sched_setaffinity has returned %d\n", ret);
	exit(-1);
      }
      cnt--;
    }
    
    ret = ts_save(TS_H1, "smp");
    if (ret) 
      rcc_error_print(stdout, ret);

    //Second pass - do not store CPU number
    cnt = 1000;

    ret = ts_close(TS_H1);
    if (ret) 
      rcc_error_print(stdout, ret);
      
    ret = ts_open(cnt, TS_H1);
    if (ret) 
      rcc_error_print(stdout, ret);

    ret = ts_start(TS_H1);
    if (ret) 
      rcc_error_print(stdout, ret);
      
    while (cnt) 
    {
#ifdef __x86_64__
      __asm__ volatile ("push %%ebx; cpuid; mov %%ebx, %%edi; pop %%ebx" : "=a" (pa), "=D" (pb), "=c" (pc), "=d" (pd) : "a" (1)); 
#endif
      //printf("APIC ID = %d\n", pb >> 24);
      
      ret = ts_record(TS_H1, 0);
      if (ret) 
        rcc_error_print(stdout, ret);    
      
      //printf("Switching to CPU mask %d...\n", (cnt % ncpus));
      ret = sched_setaffinity(0, sizeof(cpu_set_t), &cpu[(cnt % ncpus)]);
      if (ret)
      {
	printf("errno = %d\n", errno);
	printf("sched_setaffinity has returned %d\n", ret);
	exit(-1);
      }
      cnt--;
    }
    
    ret = ts_save(TS_H1, "smp2");
    if (ret) 
      rcc_error_print(stdout, ret);

    //Third pass - Print result to the screen
    cnt = 100;
    
    dmax = 0;
    dmin = 1000000000.0;
    while (cnt) 
    {
      //printf("Switching to CPU mask %d...", (cnt % ncpus));
      ret = sched_setaffinity(0, sizeof(cpu_set_t), &cpu[(cnt % ncpus)]);
      if (ret)
      {
	printf("errno = %d\n", errno);
	printf("sched_setaffinity has returned %d\n", ret);
	exit(-1);
      }

#ifdef __x86_64__
      __asm__ volatile ("push %%ebx; cpuid; mov %%ebx, %%edi; pop %%ebx" : "=a" (pa), "=D" (pb), "=c" (pc), "=d" (pd) : "a" (1)); 
#endif
      //printf("  ...APIC ID = %d\n", pb >> 24);
      
      ts_clock(&ts2);
      if (cnt < 100)
      {
        delta = ts_duration(ts1, ts2); 
        //printf("delta = %15.9f us\n", delta * 1000000); 
	if (delta < dmin)
	  dmin = delta;  
	if (delta > dmax)
	  dmax = delta;  
      }
      ts1.high = ts2.high;
      ts1.low = ts2.low;
      cnt--;
    }
    printf("smallest delta = %15.9f us\n", dmin * 1000000); 
    printf("largest delta  = %15.9f us\n", dmax * 1000000); 
    
    ret = ts_close(TS_H1);
    if (ret) 
      rcc_error_print(stdout, ret);  
#endif
  }
  
  if (mode == 7)
  {
    ret = ts_open(1, TS_DUMMY);
    if (ret) 
      rcc_error_print(stdout, ret);

    //Dummy wait to get the code cached
    printf("Wait for 1 us (code not cached)\n");
    ts_clock(&ts1);
    ts_wait(1, &yield);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf("Woken up after %15.9f us\n", delta * 1000000);   
    
    printf("\n\nWait for 1 us\n");
    ts_clock(&ts1);
    ts_wait(1, &yield);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf("Woken up after %15.9f us\n", delta * 1000000);   
    
    printf("\n\nWait for 10 us\n");
    ts_clock(&ts1);
    ts_wait(10, &yield);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf("Woken up after %15.9f us\n", delta * 1000000);   
    
    printf("\n\nWait for 100 us\n");
    ts_clock(&ts1);
    ts_wait(100, &yield);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf("Woken up after %15.9f us\n", delta * 1000000);   
        
    printf("\n\nWait for 10 ms\n");
    ts_clock(&ts1);
    ts_wait(10000, &yield);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf("Woken up after %15.9f ms\n", delta * 1000);   
    
    printf("\n\nWait for 22 ms\n");
    ts_clock(&ts1);
    ts_wait(22000, &yield);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf("Woken up after %15.9f ms\n", delta * 1000);   
    
    printf("\n\nWait for 105 ms\n");
    ts_clock(&ts1);
    ts_wait(105000, &yield);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf("Woken up after %15.9f ms\n", delta * 1000);   
    
    printf("\n\nWait for 1 s\n");
    ts_clock(&ts1);
    ts_wait(1000000, &yield);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf("Woken up after %15.9f seconds\n", delta);   
    
    ret = ts_close(TS_DUMMY);
    if (ret) 
      rcc_error_print(stdout, ret);
  }
  
  if (mode == 8)
  {
#ifdef __x86_64__
    printf("This function is not supported on 64-bit platforms because of a porting issue with an assembler instruction\n");
#endif

#ifndef __x86_64__
    ret = sched_getaffinity(0, sizeof(cpu_set_t), &mask);
    if (ret)
    {
      printf("sched_getaffinity has returned %d\n", ret);
      exit(-1);
    }

    ncpus = 0;
    for (loop = 0; loop < 32; loop++)
    {
      ret = CPU_ISSET(loop, &mask);
      printf("CPU_ISSET returns %d for CPU %d\n", ret, loop);
    
      if(ret)
      {
        CPU_ZERO(&cpu[loop]); 
        CPU_SET(loop, &cpu[loop]);
	ncpus++;
      }      
    }
    
    printf("%d CPUs found\n", ncpus);

    ret = ts_open(cnt, TS_H1);
    if (ret) 
      rcc_error_print(stdout, ret);

    cnt = 0;
    ts_clock(&ts1);
    while(1) 
    {
#ifdef __x86_64__
      __asm__ volatile ("push %%ebx; cpuid; mov %%ebx, %%edi; pop %%ebx" : "=a" (pa), "=D" (pb), "=c" (pc), "=d" (pd) : "a" (1)); 
#endif
      
      ret = sched_setaffinity(0, sizeof(cpu_set_t), &cpu[(cnt % ncpus)]);
      if (ret)
      {
	printf("errno = %d\n", errno);
	printf("sched_setaffinity has returned %d\n", ret);
	exit(-1);
      }
      ts_clock(&ts2);
      if (cnt++ < 100)
      {
        printf("APIC ID = %d, ", pb >> 24);
	printf("ts2.low = %u, ", ts2.low);
	printf("ts2.high = %u, ", ts2.high);
	printf("ts1.low = %u, ", ts1.low);
	printf("ts1.high = %u\n", ts1.high);
      }
      
      if ((ts2.high < ts1.high) || ((ts2.high == ts1.high) && (ts2.low < ts1.low)))
      {
        printf("Managed to travel back in time.\n");
	printf("ts2.low = %u\n", ts2.low);
	printf("ts2.high = %u\n", ts2.high);
	printf("ts1.low = %u\n", ts1.low);
	printf("ts1.high = %u\n", ts1.high);
      }
      
      ts1.high = ts2.high;
      ts1.low = ts2.low;
    }
    
#endif
  }
  
  if (mode == 9)
  {    
    printf("CPU benchmark\n");
    ts_open(1, TS_DUMMY);
    ts_clock(&ts1);

    for (num = sta; num < (sta + 200); num += 2)
    {
      isprim = prim(num);
      if (isprim)
      {
	printf("num = %d\n", num);
	fflush(stdout);
      }
    } 

    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2); 
    printf(" delta 1 = %15.9f seconds (%6.1f ns)\n", delta, delta * 1000000000.0);  
    ts_close(TS_DUMMY);
  } 
  
  if (mode == 10)
  {    
    struct timeval tv;
    struct timezone tz;
    
    printf("Testing automatic histogram\n");
    ts_open(1, TS_DUMMY); 
    
    for (loop = 0; loop < 200000;loop++)
    {
      ts_clock(&ts1);
      gettimeofday(&tv, &tz);    //just do do something
      ts_clock(&ts2);
      ts_histo(ts1, ts2);
    }
    ts_close(TS_DUMMY);
  }
}


/*****************/
int prim(u_int num)
/*****************/
{
  u_long fla, pf;
  double onum, wurz;

  onum = num;
  wurz = sqrt(onum);
  pf = 3;
  fla = 0;
  while( ((float)(num / pf) != (float)num / (float)pf) || (pf < (int)wurz) )
  pf += 2;
  if (num == pf)
    return(1);
  return(0);
}









