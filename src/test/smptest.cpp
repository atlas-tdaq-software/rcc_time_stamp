
/************************************************************************/
/*                                                                      */
/*   file: tstamptest.c                                                 */
/*   author: Markus Joos, CERN-EP/ESS                                   */
/*                                                                      */
/*   This is is a test program for the timestamp library.               */
/*                                                                      */
/*   History:                                                           */
/*   13.Aug.98  MAJO  created                                           */
/*    1.Mar.99  MAJO  macros introduced                                 */
/*                                                                      */
/*******Copyright 2003 - The software with that certain something********/

#include <stdlib.h>
#include <sched.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "rcc_time_stamp/my_ts_events.h"

int main(void)
{
#ifdef __x86_64__
    printf("This function is not supported on 64-bit platforms because of a porting issue with an assembler instruction\n");
#endif

#ifndef __x86_64__
  int loop, ret;
  u_int ncpus, cnt, pa, pb, pc, pd;
  cpu_set_t cpu[32], mask;
  
  ret = sched_getaffinity(0, sizeof(cpu_set_t), &mask);
  if (ret)
  {
    printf("sched_getaffinity has returned %d\n", ret);
    exit(-1);
  }

  ncpus = 0;
  for (loop = 0; loop < 32; loop++)
  {
    ret = CPU_ISSET(loop, &mask);
    printf("CPU_ISSET returns %d for CPU %d\n", ret, loop);

    if(ret)
    {
      CPU_ZERO(&cpu[loop]); 
      CPU_SET(loop, &cpu[loop]);
      ncpus++;
    }      
  }

  printf("%d CPUs found\n", ncpus);

  cnt = 10000;

  ret = ts_open(cnt, TS_H1);
  if (ret) 
    rcc_error_print(stdout, ret);

  ret = ts_start(TS_H1);
  if (ret) 
    rcc_error_print(stdout, ret);

  while (cnt) 
  {
#ifdef __x86_64__
    __asm__ volatile ("push %%ebx; cpuid; mov %%ebx, %%edi; pop %%ebx" : "=a" (pa), "=D" (pb), "=c" (pc), "=d" (pd) : "a" (1)); 
#endif

    ret = ts_record(TS_H1, pb >> 24);
    if (ret) 
      rcc_error_print(stdout, ret);    

    //printf("Switching to CPU mask %d...\n", (cnt % ncpus));
    ret = sched_setaffinity(0, sizeof(cpu_set_t), &cpu[(cnt % ncpus)]);
    if (ret)
    {
      printf("errno = %d\n", errno);
      printf("sched_setaffinity has returned %d\n", ret);
      exit(-1);
    }
    cnt--;
  }

  ret = ts_save(TS_H1, "smp");
  if (ret) 
    rcc_error_print(stdout, ret);

  ret = ts_close(TS_H1);
  if (ret) 
    rcc_error_print(stdout, ret); 
#endif 
}


 










