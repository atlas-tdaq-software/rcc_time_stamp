#include <iostream>
#include <stdlib.h>
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "rcc_error/rcc_error.h"

#define DSIZE 500000

int main(void)
{
  int ret,rol[20],hist2[20],hist[5],loop,nrols,loop2,roip,p1,p2,p3,p4,nrod,ialea;
  int next,alea[DSIZE],l0,l1,l2,l3;
  float delta,k,ft,f0,f1,f2,f3,f4;
  tstamp ts1,ts2;
  
  printf("Enter the number of ROLs: ");
  nrols=getdecd(8);
  printf("Enter the RoI request percentage per ROL: ");
  roip=getdecd(2);
  printf("The next 4 parameters have to add up to 100\n");
  printf("Enter the probability for one ROL being involved in L2PU request: ");
  p1=getdecd(25);
  printf("Enter the probability for two ROL being involved in L2PU request: ");
  p2=getdecd(25);
  printf("Enter the probability for three ROL being involved in L2PU request: ");
  p3=getdecd(25);
  printf("Enter the probability for four ROL being involved in L2PU request: ");
  p4=getdecd(25);
  if ((p1+p2+p3+p4) != 100)
    exit(-1);
  
  for(loop=0;loop<nrols;loop++)
  {
    rol[loop]=0;
    hist2[loop]=0;
  }
  hist[0]=0;
  hist[1]=0;
  hist[2]=0;
  hist[3]=0;
  hist[4]=0;
  for(loop=0;loop<DSIZE;loop++)
    alea[loop] = rand();
  
  ft=((float)p1/100.0*1.0)+((float)p2/100.0*2.0)+((float)p3/100.0*3.0)+((float)p4/100.0*4.0);
  printf("ft=%f\n",ft);
  k=((float)nrols*(float)roip)/ft;
  printf("k=%f\n",k);	
  f1=(float)p1/100.0*k;
  f2=(float)p2/100.0*k;
  f3=(float)p3/100.0*k;
  f4=(float)p4/100.0*k;
  f0=100.0-f1-f2-f3-f4;
  printf("f0 = %f\n", f0);
  printf("f1 = %f\n", f1);
  printf("f2 = %f\n", f2);
  printf("f3 = %f\n", f3);
  printf("f4 = %f\n", f4);

  l0=(int)(f0/100.0*(float)RAND_MAX);
  l1=(int)((f0+f1)/100.0*(float)RAND_MAX);
  l2=(int)((f0+f1+f2)/100.0*(float)RAND_MAX);
  l3=(int)((f0+f1+f2+f3)/100.0*(float)RAND_MAX);
  
  printf("l0 = %d\n", l0);
  printf("l1 = %d\n", l1);
  printf("l2 = %d\n", l2);
  printf("l3 = %d\n", l3);
 
  printf("Now generating 1000000 L1IDs\n");
  printf("Expected number of ROD fragment requests = %d\n",10000*roip*nrols);
 
  //measure speed
  ret = ts_open(1,TS_DUMMY);
  ret = ts_clock(&ts1);
  next=0;
  for(loop=0;loop<1000000;loop++)
  {
    //define the number of ROLs involved for this event
    ialea=alea[next];
    if (++next == DSIZE) next = 0;
    if(ialea < l0) nrod=0;
    else if(ialea < l1) nrod=1;
    else if(ialea < l2) nrod=2;
    else if(ialea < l3) nrod=3;
    else nrod=4;
    hist[nrod]++;
    
    //define the ROLs involved for this event
    ialea=alea[next]%nrols;
    if (++next == DSIZE) next = 0;
    for(loop2=0;loop2<nrod;loop2++)
    {
      hist2[ialea]++;
      ialea++;
      if (ialea==nrols)
        ialea=0;
    }
  }
  ret = ts_clock(&ts2);
  
  printf("\n\nOne million L1IDs generated\n");
  printf("=======================================\n");
  delta = ts_duration(ts1,ts2);
  printf("Time spent in trigger generation loop = %f s\n", delta);
  printf("\n\n\n");
  ts_close(TS_DUMMY);
  
  printf("Statistics for: number of ROLs per L2PU request\n");
  printf("Events without L2PU request          = %d\n",hist[0]);
  printf("Events with L2PU request from 1 ROL  = %d\n",hist[1]);
  printf("Events with L2PU request from 2 ROLs = %d\n",hist[2]);
  printf("Events with L2PU request from 3 ROLs = %d\n",hist[3]);
  printf("Events with L2PU request from 4 ROLs = %d\n",hist[4]);
  
  printf("\nStatistics for: number of ROLs per L2PU request\n");
  for(loop=0;loop<nrols;loop++)
    printf("Events requested from ROL %d = %d\n", loop, hist2[loop]);

  printf("\nTotal number of ROD fragments requested = %d\n", hist[1]+hist[2]*2+hist[3]*3+hist[4]*4);
  



}






