
/************************************************************************/
/*                                                                      */
/*   file: tsonline.c                                                   */
/*   author: Markus Joos, CERN-EP/ESS                                   */
/*                                                                      */
/*   This is is a test program for the timestamp library.               */
/*                                                                      */
/*   History:                                                           */
/*   10.Mar.99  MAJO  created                                           */
/*   26.Jul.99  JOP   let it run a bit longer ..                        */
/*                                                                      */
/*******Copyright 1999 - The software with that certain something********/
 
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "rcc_error/rcc_error.h"

//globals
tstamp m_ts1;


/*****************/
bool checkAge(void)
/*****************/
{
  unsigned int m_maxAge = 10000;

  tstamp ts2;

  if (m_maxAge == 0) //time-out disabled
    return(false);

  int ret = ts_clock(&ts2);
  if (ret)
  {
    printf("error 1\n");
    return(true);
  }

  unsigned int myAge = (int)(1000000.0 * ts_duration(m_ts1, ts2));
  if (myAge > m_maxAge)
  {
    printf("error 2\n");
    return(true);
  }
  else
    return(false);
}


 
int main(void)
{
  int ret;
  tstamp ts1, ts2, ts3, ts4;
  float delta;
  unsigned int i = 0, usecs, num, loop, nsecs, nyield;
  unsigned int dest[256], src[256];

  ret = ts_open(1, TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);

  printf("Timing checkAge\n");
  ret = ts_clock(&ts1);
  for(loop = 0; loop < 1000000; loop++)
  {
    ret = ts_clock(&m_ts1);
    checkAge();
  }
  ret = ts_clock(&ts4);
  delta = ts_duration(ts1, ts4);
  printf("1000000 checkAge = %f seconds\n", delta);

  printf("Testing memcpy\n");
  ret = ts_clock(&ts1);
  memcpy(dest, src, 1024);
  ret = ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  printf(" The first (uncached ?) memcpy of 1 KB takes %f us\n", delta * 1000000.0);

  ret = ts_clock(&ts1);
  memcpy(dest, src, 1024);
  ret = ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  printf(" The second (cached ?) memcpy of 1 KB takes %f us\n\n", delta * 1000000.0);

  printf("Testing ts_wait\n");
  printf("Enter the number of us to wait: ");
  usecs = getdecd(10000);
  ret = ts_clock(&ts1);
  if (ret)
    rcc_error_print(stdout, ret);

  ret = ts_wait(usecs, &nyield);
  if (ret)
    rcc_error_print(stdout, ret);

  ret = ts_clock(&ts2);
  if (ret)
    rcc_error_print(stdout, ret);
  delta = ts_duration(ts1, ts2);
  printf(" Program has waited for %f us\n", delta * 1000000.0);
  printf(" Program went to sleep %d times whilst waiting\n", nyield);

  printf("Testing ts_wait_until and ts_offset\n");
  printf("Enter the number of us to wait: ");
  usecs = getdecd(10000);
  ret = ts_clock(&ts1);
  if (ret)
    rcc_error_print(stdout, ret);

  ret = ts_clock(&ts3);
  if (ret)
    rcc_error_print(stdout, ret);

  ret = ts_offset(&ts3, usecs);
  if (ret)
    rcc_error_print(stdout, ret);

  ret = ts_wait_until(ts3, &nyield);
  if (ret)
    rcc_error_print(stdout, ret);

  ret = ts_clock(&ts2);
  if (ret)
    rcc_error_print(stdout, ret);
  delta = ts_duration(ts1, ts2);
  printf(" Program has waited for %f us\n", delta * 1000000.0);
  printf(" Program went to sleep %d times whilst waiting\n", nyield);


  printf("Testing ts_delay\n");
  printf("Enter the number of us to sleep: ");
  usecs = getdecd(5);
  num = 1000000 / usecs;
  ret = ts_clock(&ts1);
  if (ret)
    rcc_error_print(stdout, ret);
  for(loop = 0; loop < num; loop++)
  { 
    ret = ts_delay(usecs);
    if (ret)
      rcc_error_print(stdout, ret);
  }
  ret = ts_clock(&ts2);
  if (ret)
    rcc_error_print(stdout, ret);
  delta = ts_duration(ts1, ts2);
  printf(" Program has been delayed %d times for %d us (=%d us)\n", num, usecs, num * usecs);
  printf(" measured total delay = %d usecs\n", (unsigned int)(delta * 1000000.0));   

  printf("\nTesting ts_clock and ts_duration\n");
  printf("Enter # of seconds to sleep");
  nsecs = getdecd(10);

  ret = ts_clock(&ts1);
  if (ret)
    rcc_error_print(stdout, ret);

  for (i = 0; i < nsecs; i++) 
  {
    printf("Sleeping one second\n");
    sleep(1);
    ret = ts_clock(&ts2);
    if (ret)
      rcc_error_print(stdout, ret);
    delta = ts_duration(ts1, ts2); 
    printf(" i=%d   Woken up after %15.9f seconds\n", i, delta);   
    printf("t1.high=0x%8x  t1.low=0x%8x\n", ts1.high, ts1.low);
    printf("t2.high=0x%8x  t2.low=0x%8x\n", ts2.high, ts2.low);
  }

  ret = ts_close(TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);
  return 0;
}

