This package has been developed in ~2002 in order to measure time precisely. It is based on an assembler instruction that reads the time stamp counter of the local CPU. While it has worked well for the first generation of VMEbus SBCs and ROS PCs, it has to be used with care on modern multi-core CPUs. In order to get proper time measurements and delays you have to:
- disable the performance governor of the CPU (I.e. run the CPU at the maximum frequency)
- pin the process to a single core (the TSCs of different cores can drift)

Please consider  not to use this package but to use the modern high resolution timers of Linux
